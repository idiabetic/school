require 'rails_helper'

describe 'user visits course page' do
  it 'should show all the student' do
    student_1 = Student.create!(name: 'Bart')
    student_2 = Student.create!(name: 'List')
    student_3 = Student.create!(name: 'Ralph')

    course = Course.create!(title: 'Math')
    student_1.courses << course
    student_2.courses << course
    student_3.courses << course


    visit course_path(course)

    expect(page).to have_content(course.title)
    expect(page).to have_content(student_1.name)
    expect(page).to have_content(student_2.name)
    expect(page).to have_content(student_3.name)
  end
end
