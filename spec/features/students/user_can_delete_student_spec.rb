require 'rails_helper'

describe 'User Clicks A Delete Button' do
  before(:each) do
    @name = 'Bart'
    @student = Student.create!(name: @name)
  end
  scenario 'User deletees a student' do
   visit student_path(@student)

   click_on 'Delete'

   expect(current_path).to eq(students_path)
   expect(page).to_not have_content(@name)
  end
end
