require 'rails_helper'

describe 'Vistit Student index page' do
  before(:each) do
    names = ['Paul', 'Brian', 'Samantha', 'Rudy']
    @student_1 = Student.create!(name: names.sample)
    @student_2 = Student.create!(name: names.sample)
    @student_3 = Student.create!(name: names.sample)
    @student_4 = Student.create!(name: names.sample)
    @student_5 = Student.create!(name: names.sample)
  end
  scenario 'user can see navigation links' do
    visit students_path

    expect(page).to have_link('All Students')
    expect(page).to have_link('Create A New Student')
  end
  scenario 'User should see all of the student names' do

    visit students_path

    expect(page).to have_link(@student_1.name)
    expect(page).to have_link(@student_2.name)
    expect(page).to have_link(@student_3.name)
    expect(page).to have_link(@student_4.name)
    expect(page).to have_link(@student_5.name)
  end
end
