require 'rails_helper'

describe 'user visits the student edit page' do
  before(:each) do
    @name = 'Bart'
    @student = Student.create!(name: @name)
  end
  scenario 'user edits a student' do
    new_name = 'Lisa'
    visit edit_student_path(@student)

    expect(page).to have_content(@name)

    fill_in 'student[name]', with: new_name
    click_on 'submit'

    expect(page).to have_content(new_name)
  end
end
