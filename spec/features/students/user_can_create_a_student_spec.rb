require 'rails_helper'

describe 'User visits The student Create Page' do
  scenario 'User creates a new student' do
    name = 'Bart'
    visit new_student_path

    expect(page).to have_content('Create a New Student')

    fill_in 'student[name]', with: name
    click_on 'submit'

    expect(page).to have_content(name)
  end
end
