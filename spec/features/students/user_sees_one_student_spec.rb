require 'rails_helper'

describe 'User visits Student Show Page' do
  before(:each) do
    @student = Student.create(name: 'Betty')
    description = 'Moms house'
    street = '12345 Evergreen St'
    city = 'Evergreen'
    state = 'CO'
    zip = '80123'
    @address = @student.addresses.create!(description: description,
                                          street: street,
                                          city: city,
                                          state: state,
                                          zip_code: zip
                                          )
    @course_1 = 'Math'
    @course_2 = 'English'
    @course_3 = 'Comp Sci'
    @student.courses.create!(title: @course_1)
    @student.courses.create!(title: @course_2)
    @student.courses.create!(title: @course_3)

  end
  scenario 'user visits a students show page' do
    visit student_path(@student)

    expect(page).to have_content(@student.name)
    expect(page).to have_content(@address.description)
    expect(page).to have_content(@address.street)
    expect(page).to have_content(@address.city)
    expect(page).to have_content(@address.state)
    expect(page).to have_content(@address.zip_code)
    expect(page).to have_content(@course_1)
    expect(page).to have_content(@course_2)
    expect(page).to have_content(@course_3)

  end
end
