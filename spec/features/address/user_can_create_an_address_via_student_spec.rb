require 'rails_helper'

describe 'user visits the student new address link' do
  it 'should be a nested route and have a form' do
    name = 'Ralph'
    student = Student.create!(name: name)
    description = 'Moms house'
    street = '12345 Evergreen St'
    city = 'Evergreen'
    state = 'CO'
    zip = '80123'
    visit new_student_address_path(student)

    expect(page).to have_content("Add a New Address for #{name}")

    fill_in 'address[description]', with: description
    fill_in 'address[street]', with: street
    fill_in 'address[city]', with: city
    fill_in 'address[state]', with: state
    fill_in 'address[zip_code]', with: zip
    click_on 'submit'

    expect(page).to have_content(description)
    expect(page).to have_content(street)
    expect(page).to have_content(city)
    expect(page).to have_content(state)
    expect(page).to have_content(zip)

  end
end
