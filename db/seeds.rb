# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
NAMES = ['Brian', 'Paul', 'Steven', 'Nancy', 'Erica', 'Ben', 'Summer']
COURSES = ['Math', 'Science', 'Compsci']

25.times do
    student = Student.create(name: NAMES.sample)
    student.courses.create(title: COURSES.sample)
    student.courses.create(title: COURSES.sample)
    student.courses.create(title: COURSES.sample)
end
